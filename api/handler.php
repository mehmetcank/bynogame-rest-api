<?php 
    
    $conn = mysqli_connect("localhost","root", "","bynogame"); // VT Bağlantısı
    
    if($_SERVER['REQUEST_METHOD'] == 'POST') // isteğimiz post methodu iste bu işlemleri yap
    {
        if( isset($_POST['product_id']) && is_numeric($_POST['product_id']) && isset($_POST['name']) && isset($_POST['stock']) && is_numeric($_POST['stock']))
        {
            
            $product_id = $conn->real_escape_string($_POST['product_id']);
            $name       = $conn->real_escape_string($_POST['name']);
            $stock      = $conn->real_escape_string($_POST['stock']);           
            
            $conn->query("Insert INTO products(product_id,name,stock,created_date) VALUES ('$product_id','$name','$stock',NOW())");                
            
            $data = array
            (
                "code"          => 0,
                "msg"           => "success",
                "data"          => array(
                "product_id"    => $product_id,
                "name"          => $name,
                "stock"         => $stock,
                "created_date"  => date("Y-m-d H:i:s")
                )
            );
            exit(json_encode($data));            
        }        
        else 
        {
            exit(json_encode(array("code" => error_reporting(-1), 'msg' => 'Bir hata ile karsilastim. Uzgunum ekleme islemini gerceklestiremedim.')));
        }        
    } 
    else if ($_SERVER['REQUEST_METHOD'] == 'GET') 
    {
        if(isset($_GET['id'])){
            
            $data = array();
            $id = $conn->real_escape_string($_GET['id']);
            $sql = $conn->query("Select * from products where id='$id'");
            $data = $sql->fetch_assoc();
            
            $data = array(
                "code"          =>0,
                "msg"           =>"success",
                "data"          => $data
            );
        }
         else {
            $data = array();
            
            $sql = $conn->query("Select * from products");
            while($d = $sql->fetch_assoc()) {
                $data[] = $d;
            }
            $data = array(
                "code"       =>0,
                "msg"        =>"success",
                "data"      => $data
            );
        }

        exit(json_encode($data));
    }
?>