-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 16 Kas 2019, 01:32:39
-- Sunucu sürümü: 10.4.6-MariaDB
-- PHP Sürümü: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `bynogame`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `products`
--

INSERT INTO `products` (`id`, `product_id`, `name`, `stock`, `created_date`) VALUES
(1, 0, 'BNG Product', 35, '2019-11-16 16:16:16'),
(2, 0, 'BNG Product2', 5, '2019-11-16 18:19:20'),
(3, 1, 'bynogame deneme', 25, '2019-11-16 01:28:42'),
(4, 1, 'bynogame deneme', 25, '2019-11-16 01:32:42'),
(5, 1, 'bynogame deneme', 25, '2019-11-16 01:33:18'),
(6, 1, 'bynogame deneme', 25, '2019-11-16 01:35:54'),
(7, 1, 'bynogame deneme', 25, '2019-11-16 01:39:36'),
(8, 1, 'bynogame deneme', 25, '2019-11-16 01:40:40'),
(9, 1, 'bynogame deneme', 25, '2019-11-16 01:43:53'),
(10, 1, 'bynogame deneme', 25, '2019-11-16 01:44:40'),
(11, 0, 'bynogame deneme', 0, '2019-11-16 01:55:10'),
(12, 15, 'Ã¼rÃ¼n', 25, '2019-11-16 02:11:35'),
(13, 15, 'deneme', 25, '2019-11-16 02:11:50'),
(14, 15, 'deneme', 25, '2019-11-16 02:18:59'),
(15, 22, '22', 2, '2019-11-16 02:28:00'),
(16, 22, 'name', 2, '2019-11-16 02:28:08'),
(17, 0, 'name', 2, '2019-11-16 02:28:13'),
(18, 2, 'name', 2, '2019-11-16 02:28:29'),
(19, 2, 'name', 2, '2019-11-16 02:28:54'),
(20, 0, 'name', 2, '2019-11-16 02:28:59'),
(21, 0, 'name', 0, '2019-11-16 02:29:02'),
(22, 2, '2', 2, '2019-11-16 02:31:44'),
(23, 0, '2', 2, '2019-11-16 02:31:51'),
(24, 0, '2', 2, '2019-11-16 02:32:30'),
(25, 0, '2', 2, '2019-11-16 02:37:27'),
(26, 11, '2', 2, '2019-11-16 02:37:46'),
(27, 11, '2', 2, '2019-11-16 02:38:31'),
(28, 0, '2', 2, '2019-11-16 02:38:35'),
(29, 0, '2', 2, '2019-11-16 02:53:53'),
(30, 0, '2', 2, '2019-11-16 02:54:15'),
(31, 0, '2', 2, '2019-11-16 02:56:10'),
(32, 1, '2', 2, '2019-11-16 02:56:25'),
(33, 1, '2', 2, '2019-11-16 02:57:15'),
(34, 2, '2', 2, '2019-11-16 02:57:19'),
(35, 1, 'deneme', 2, '2019-11-16 02:57:49'),
(36, 1, 'deneme', 2, '2019-11-16 03:00:25');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
